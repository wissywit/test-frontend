import {useCallback, useMemo} from "react";

const useColumn = () => {
    const columns = useMemo(() => [
       {
            Header: 'Image',
            accessor: 'image',
            Cell: ({ cell: { value } }) => {
                return <img src={value} width={50} alt="thumbnail"/>
            },
            canFilter: false
        },
        {
            Header: 'Title',
            accessor: 'title',
            canFilter: false
        },
        {
            Header: 'Vendor',
            accessor: 'vendor',
            canFilter: false
        },
        {
            Header: 'Option',
            accessor: 'option',
            canFilter: false
        },
        {
            Header: 'Price',
            accessor: 'price',
            canFilter: false
        },
        {
            Header: 'Published',
            accessor: 'published',
            Cell: ({ cell: { value } }) => {
                return value ? 'Yes' : 'No'
            },
            canFilter: false
        },
        {
            Header: 'Slug',
            accessor: 'slug',
            canFilter: false,
        },
        {
            Header: 'Subscription',
            accessor: 'subscription',
            Cell: ({ cell: { value } }) => {
                return value ? 'Yes' : 'No'
            },
            canFilter: false
        },
        {
            Header: 'Sub Discount',
            accessor: 'subscriptionDiscount',
            canFilter: false,
            Cell: ({ cell: { value } }) => {
                return value === "" ? 'Nil' : value
            },
        },
        {
            Header: 'Tags',
            accessor: 'tags_like',
            Cell: ({ cell: { value } }) => {
                return value.join(', ')
            },
            canFilter: false
        },
    ], []);

    const formatData = useCallback((data) => {
        const result = []
        data.forEach((datum) => {
            result.push({
                image: datum.image_src,
                title: datum.title,
                vendor: datum.vendor,
                option: datum.option_value,
                price: datum.price,
                published: datum.published,
                slug: datum.slug,
                subscription: datum.subscription,
                subscriptionDiscount: datum.subscription_discount,
                tags_like: datum.tags
            })
        })
        return result
    }, [])
    return [columns, formatData]
}

export default useColumn
