import React, {useState} from "react";
import AppContext from "./appContext";

const AppProvider = ({children}) => {
    const [filterParams, setFilterParams] = useState({})
    const value = {
        filterParams,
        setFilterParams
    }
    return <AppContext.Provider value={value}>{children}</AppContext.Provider>
}

export default AppProvider
