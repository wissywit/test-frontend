export const arrToQueryString = arr => {
    return arr.map(el => encodeURIComponent(el.id) + '=' + encodeURIComponent(el.value)).join('&')
}
