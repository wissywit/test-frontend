import axios from 'axios'
import Nprogress from 'nprogress'
import 'nprogress/nprogress.css'
import { BASE_URL } from './constants'

const apiClient = axios.create({
    baseURL: BASE_URL,
    headers: {
        Accept: 'application/json'
    }
})


apiClient.interceptors.request.use(config => {
    Nprogress.start()
    return config
})

apiClient.interceptors.response.use(
    response => {
        Nprogress.done()
        return response
    },
    error => {
        Nprogress.done()
        return Promise.reject(error)
    }
)

export { apiClient }
