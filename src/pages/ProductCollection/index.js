import React, {useCallback, useContext, useState} from "react";
import useColumn from "../../hooks/useColumn";
import {apiClient} from "../../service";
import {arrToQueryString} from "../../utils";
import Table from "../../components/Table";
import './style.scss'
import AppContext from "../../context/appContext";
import Filter from "../../components/Filter";

const ProductCollection = () => {
    const { filterParams } = useContext(AppContext)

    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false)
    const [pageCount, setPageCount] = useState(0)
    const [columns, formatData] = useColumn();
    const [totalCount, setTotalCount] = useState(0);

    const fetchData = useCallback(
        ({pageSize, pageIndex, filters}) => {
            setLoading(true)
            apiClient.get(`/products?_page=${pageIndex + 1}&_limit=${pageSize}&${arrToQueryString(filters)}`).then(res => {
                setPageCount(Math.ceil(parseInt(res.headers['x-total-count'])/pageSize))
                setLoading(false)
                setData(formatData(res.data))
                setTotalCount(parseInt(res.headers['x-total-count']))
            }).catch(console.log)
        },
        [formatData],
    );
    return (
        <div className="container">
            <div className="content">
                <h2>Products</h2>
                <Table
                    data={data}
                    totalCount={totalCount}
                    fetchData={fetchData}
                    loading={loading}
                    pageCount={pageCount}
                    columns={columns}
                    filterParams={filterParams}/>
            </div>
            <Filter/>
        </div>
    )
}

export default ProductCollection
