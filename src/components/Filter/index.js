import React, {useContext, useRef} from "react";
import searchIcon from "../../assets/svgs/search.svg";
import './style.scss'
import debounce from 'lodash.debounce'
import AppContext from "../../context/appContext";


const Filter = () => {
    const { setFilterParams, filterParams } = useContext(AppContext)
    const filterRef = useRef(null)
    const radioOneRef = useRef(null)
    const radioTwoRef = useRef(null)

    const handleReset = () => {
        setFilterParams(prevParams => {
            delete prevParams['subscription']
            return { ...prevParams }
        })
        if(radioOneRef && radioTwoRef){
            radioOneRef.current.checked = false;
            radioTwoRef.current.checked = false;
        }
    }
    const handleChange = debounce(({target}) => {
        const {value, name} = target
        if(value.trim() === '') {
            setFilterParams(prevParams => {
                delete prevParams[name]
                return { ...prevParams }
            })

            return
        }

        setFilterParams(prevParams => {
            return { ...prevParams, [name]: value }
        })
    }, 1000)

    const clearFilter = () => {
        if (Object.keys(filterParams).length > 0) {
            setFilterParams({})
        }
        filterRef.current.reset()
    }
    return (
        <form className="filter__container" ref={filterRef} onSubmit={(e) => e.preventDefault()}>
            <div className="header">
                <h2>Filters</h2>
                {Object.keys(filterParams).length > 0 && (
                    <button onClick={clearFilter}>Clear</button>
                )}
            </div>
            <hr/>
            <div className="filter__content">
                <p>Tags</p>
                <div className="input__group">
                    <img src={searchIcon} alt=""/>
                    <input type="text" name="tags_like"  placeholder="Search" onChange={handleChange}/>
                </div>
            </div>
            <hr/>
            <div className="filter__content">
                <p>Price</p>
                <div className="input__group">
                    <img src={searchIcon} alt=""/>
                    <input type="text" name="price" placeholder="Search" onChange={handleChange}/>
                </div>
            </div>
            <hr/>
            <div className="filter__content">
                <div className="subscription">
                    <p>Subscription</p>
                    {filterParams.hasOwnProperty('subscription') && (
                        <button onClick={handleReset}>RESET</button>
                    )}
                </div>

                <div className="radio__group" onChange={handleChange} >
                    <div>
                        <input type="radio" value={true} name="subscription" id="yes" ref={radioOneRef}/>
                        <label htmlFor="yes">Yes</label>
                    </div>
                    <div>
                        <input type="radio" value={false} name="subscription" id="no" ref={radioTwoRef}/>
                        <label htmlFor="no">No</label>
                    </div>
                </div>
            </div>
            <hr/>
        </form>
    )
}

export default Filter
