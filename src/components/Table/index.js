import React, {useEffect} from "react";
import {useTable, usePagination, useFilters} from "react-table";
import './style.scss'

const Table = ({ columns, data, fetchData, loading, pageCount: controlledPageCount, filterParams, totalCount }) => {
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setAllFilters,
        // setPageSize,
        // Get the state from the instance
        state: { pageIndex, pageSize, filters },
    } = useTable(
        {
            columns,
            data,
            initialState: { pageIndex: 0, pageSize: 8 }, // Pass our hoisted table state
            manualPagination: true, // Tell the usePagination
            // hook that we'll handle our own data fetching
            // This means we'll also have to provide our own
            // pageCount.
            pageCount: controlledPageCount,
            manualFilters: true,

        },
        useFilters,
        usePagination,
    )

    useEffect(() => {
        fetchData({ pageIndex, pageSize, filters })
    }, [fetchData, pageIndex, pageSize, filters])

    useEffect(() => {
        const allFilters = []
        Object.keys(filterParams).forEach(key => {
            allFilters.push({id: key, value: filterParams[key]})
        })
        setAllFilters(allFilters)
    }, [filterParams, setAllFilters])

    console.log(filters)

    return (
        <>
            <table {...getTableProps()}>
                <thead>
                {headerGroups.map(headerGroup => (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                        <th>S/N</th>
                        {headerGroup.headers.map(column => (
                            <th {...column.getHeaderProps()}>
                                {column.render('Header')}
                                <span>
                    {column.isSorted
                        ? column.isSortedDesc
                            ? ' 🔽'
                            : ' 🔼'
                        : ''}
                  </span>
                            </th>
                        ))}
                    </tr>
                ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                {page.map((row, i) => {
                    prepareRow(row)
                    return (
                        <tr {...row.getRowProps()}>
                            <td>{i+1}</td>
                            {row.cells.map(cell => {
                                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                            })}
                        </tr>
                    )
                })}
                {page.length === 0 && (
                    <tr>
                        <td colSpan="100000" align="center">No records found</td>
                    </tr>
                )}
                <tr>
                    {loading ? (
                        // Use our custom loading state to show a loading indicator
                        <td colSpan="10000">Loading...</td>
                    ) : (
                        <td colSpan="10000">
                            Showing {page.length} of {totalCount}{' '}
                            results
                        </td>
                    )}
                </tr>
                </tbody>
            </table>
            <div className="pagination">
                <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
                    {'<<'}
                </button>{' '}
                <button onClick={() => previousPage()} disabled={!canPreviousPage}>
                    {'<'}
                </button>{' '}
                <button onClick={() => nextPage()} disabled={!canNextPage}>
                    {'>'}
                </button>{' '}
                <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
                    {'>>'}
                </button>{' '}
                <span>
                  Page{' '} <strong> {pageIndex + 1} of {pageOptions.length}</strong>
                </span>
            </div>
        </>
    )
}

export default Table
