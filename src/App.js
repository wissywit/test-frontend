import ProductCollection from "./pages/ProductCollection";
import AppProvider from "./context/AppProvider";

function App() {


  return (
    <AppProvider>
        <ProductCollection/>
    </AppProvider>
  );
}

export default App;
