SOLUTION
========

Estimation
----------
Estimated: 6 hours

Spent: 7 hours


Solution
--------
Some Product requirements are not in correspondent with the available products.json contents, and as such it doesn't give exactly what was given on the README file, but instead filters the exact and expected products according to the selected filters.

To run all test cases based on the Conditions of Acceptance: run `yarn start` and then run `yarn run e2e`. (*make sure the backend server is running*).

For better product of the task I would adopt typescript, it reduces the level of bugs due to syntax or typo, basically for correctness. Also, I could add more filters to further break down to granular searching of products.
