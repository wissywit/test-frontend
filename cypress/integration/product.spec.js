describe('Product Collection Tests', () => {
    beforeEach(() => {
        cy.visit('/')
    })

    it('loads filter sidebar correctly', () => {
        cy.get('.filter__content').should('have.length', 3)
        cy.get('.filter__content').first().get('p').first().should('have.text', 'Tags')
    })

    it('displays table showing 8 products with pagination', () => {
        cy.intercept('GET', '**/products?*').as('getProducts')
        cy.wait('@getProducts').its('response.statusCode').should('be.oneOf', [200, 304])
        cy.get('table tbody tr').should('have.length', 9)
        cy.get('.pagination button').should('have.length', 4)

        // Listen to GET to products?_page=2&_limit=8
        cy.intercept('GET', '**/products?*').as('getNextProducts')

        //Click last button of pagination
        cy.get('.pagination button').last().click()

        // Get the last 3 products
        cy.wait('@getNextProducts').its('response.statusCode').should('be.oneOf', [200, 304])
        cy.get('table tbody tr').should('have.length', 4)
    })

    it('Search the Tag input for "Cat" and subscription "Yes" and displays 5 records on the Table', () => {
        cy.get('input[name="tags_like"]').type('Cat').should('have.value', 'Cat')

        // Listen to GET to products?_page=2&_limit=8
        cy.intercept('GET', '**/products?*').as('getFilteredProducts')

        // Get the filtered rows
        cy.wait('@getFilteredProducts').its('response.statusCode').should('be.oneOf', [200, 304])
        cy.get('table tbody tr').should('have.length', 7)


        //Check yes for subscription
        cy.get('.radio__group div').first().get('label').first().click()

        // Get the filtered rows
        cy.wait('@getFilteredProducts').its('response.statusCode').should('be.oneOf', [200, 304])
        cy.get('table tbody tr').should('have.length', 6)

    })

    it('filter by "Price" "127" in the sidebar and returns 4 records', () => {
        cy.get('input[name="price"]').type('127').should('have.value', '127')

        // Listen to GET to products?_page=2&_limit=8
        cy.intercept('GET', '**/products?*').as('getFilteredProducts')

        // Get the filtered rows
        cy.wait('@getFilteredProducts').its('response.statusCode').should('be.oneOf', [200, 304])
        cy.get('table tbody tr').should('have.length', 5)
    })
})
